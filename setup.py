__author__ = 'lux'

from setuptools import setup

setup(
    name='sugar-router',
    version='0.0.1',
    author='lux',
    author_email='lux@sugarush.io',
    url='https://gitlab.com/sugarush/sugar-router',
    packages=[
        'sugar_router'
    ],
    description='A asynchronous event router.',
    install_requires=[
        'sugar-asynctest@git+https://gitlab.com/sugarush/sugar-asynctest@master'
    ]
)
